Ejecutar el comando de Composer para agregar la dependencia y el archivo Composer.json

composer require google/apiclient:^2.0

Generar las credenciales en la documentacion de Google Calendar

https://developers.google.com/calendar/quickstart/php?authuser=3

Agregar el archivo credentials.json generado a la raiz del proyecto y luego ejecutar el siguiente comando

php index.php
